'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RolVistaSchema extends Schema {
  up () {
    this.create('rol_vistas', (table) => {
      table.increments()
      table.integer('rol_id').unsigned().references('id').inTable('rols')
      table.integer('vista_id').unsigned().references('id').inTable('vistas')
      table.timestamps()
    })
  }

  down () {
    this.drop('rol_vistas')
  }
}

module.exports = RolVistaSchema
  