'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VistaSchema extends Schema {
  up () {
    this.create('vistas', (table) => {
      table.increments()
      table.string('name', 50).notNullable()
      table.string('nivel').nullable()
      table.string('ruta').nullable()
      table.string('categoria_id').nullable()
      table.string('icono').nullable()
      table.integer('categoria_id').unsigned().references('id').inTable('categoria')
      table.boolean('status').defaultTo(0)
      table.timestamps()
    })
  }

  down () {
    this.drop('vistas')
  }
}

module.exports = VistaSchema
