'use strict'
/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('A', () => {return { greeting: 'Funciona rutas User' }
}).prefix('api/v1')


Route.group(() => {
    Route.resource('User', 'UserController')
    .apiOnly()
    .except(['store'])
    .validator(new Map([
      [['users.update'], ['UpdateUser']]
    ]))
})
.prefix('api/v1')
.namespace('User')
.middleware(['auth'])
  
