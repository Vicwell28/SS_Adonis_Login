'use strict'
/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('B', () => {return { greeting: 'Funciona rutas Rol' }
}).prefix('api/v1')


Route.group(() => {
    Route.resource('Rol', 'RolController')
    .apiOnly().
    validator(new Map([
      [['Rol.store'], ['StoreRol']]
    ]))
})
.prefix('api/v1')
.namespace('User')
.middleware(['auth'])
  
  