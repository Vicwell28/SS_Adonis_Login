'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */

const Route = use('Route')
const routesRol = use('./routes/routesRol')
const routesUser = use('./routes/routesUser')
const routesVista = use('./routes/routesVista')
const routesAuth = use('./routes/routesAuth')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})


