'use strict'
const Hash = use('Hash')

const UserHook = exports = module.exports = {}

// UserHook.method = async (modelInstance) => {
// }

UserHook.hashPassword = async (userInstance) => {
    if (userInstance.dirty.password) {
      userInstance.password = await Hash.make(userInstance.password)
    }
  }
  
