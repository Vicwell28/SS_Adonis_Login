'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Rol extends Model {
    static get store(){
        return ['name']
    }

    static get table () {
    return 'rols'
  }

  static get primaryKey () {
    return 'id'
  }

    static get hidden () {
        return ['created_at', 'updated_at']
    }

    User () {
        return this.hasMany('App/Models/User', 'id', 'rol_id')
    }    
    
}

module.exports = Rol
