'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Categoria = use('App/Models/Categoria');

/**
 * Resourceful controller for interacting with categorias
 */
class CategoriaController {
  /**
   * Show a list of all categorias.
   * GET categorias
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
   async index ({ response, auth }) {
    const categorias = await Categoria.all()
    
    return response.ok({
      "status" : true, 
      "massage" : "El Usuario Fue Registrado Exitosamente", 
      "data" : categorias
    })
}

/**
 * Create/save a new categoria.
 * POST categorias
 * @param {object} ctx
 * @param {Request} ctx.request
 * @param {Response} ctx.response
 */
async store ({ request, response }) { 
  const categoriaData = request.only(Categoria.store);

  const categoria = await Categoria.create(categoriaData);

  return response.ok({
    "status" : true, 
    "massage" : "Te has registrado con exito.", 
    "data" : categoria
  });
}

/**
 * Display a single categoria.
 * GET categorias/:id
 *
 * @param {object} ctx
 * @param {Request} ctx.request
 * @param {Response} ctx.response
 * @param {View} ctx.view
 */
async show ({ params, request, response, auth }) {

  try{
    const a =  auth.categoria
    const b = await a.with('Rol').fetch()      
  
    return response.ok({
      "status" : true, 
      "massage" : "Tu Usuario Fue Encontrado Con Exito", 
      "data" : b
    })
  }
  catch(error){
    return response.status(500).json({
      "status" : false, 
      "massage" : "Tu Usuario No Fue Encontrado", 
      "data" : error
    })
  }
  

}

/**
 * Update categoria details.
 * PUT or PATCH categorias/:id
 *
 * @param {object} ctx
 * @param {Request} ctx.request
 * @param {Response} ctx.response
 */
async update ({ auth, request, response, params }) {
 const categoriadata =  await Categoria.findOrFail(auth.categoria.id)

  const inputs = request.only(['categorianame', 'email', 'password'])

  categoriadata.categorianame = inputs.categorianame; 
  categoriadata.email = inputs.email; 
  categoriadata.password = inputs.password; 

  categoriadata.save();

  const json = categoriadata.toJSON()

  return response.ok({
    "status" : true, 
    "massage" : "Tu Usuario Fue Actualizado Correctamente", 
    "data" : json
  })
}

/**
 * Delete a categoria with id.
 * DELETE categorias/:id
 *
 * @param {object} ctx
 * @param {Request} ctx.request
 * @param {Response} ctx.response
 */
async destroy ({ auth, request, response }) {
  
  const categoriadata = await Categoria.findOrFail(auth.categoria.id);

  categoriadata.soft_delete = !categoriadata.soft_delete; 

  categoriadata.save();

  const json = categoriadata.toJSON();

  return response.ok({
    "status" : true, 
    "massage" : "Tu Usuario Fue Eliminado Correctamente", 
    "data" : json
  })
}

}

module.exports = CategoriaController
