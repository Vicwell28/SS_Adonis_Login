'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const User = use('App/Models/User');
const Rol = use('App/Models/Rol');

class AuthController {

  async login ({ auth, request, response }) {

    const { email, password } = request.all()
    const resul =  await auth.withRefreshToken().attempt(email, password); 

    return response.ok({
      "status" : true, 
      "massage" : "El Usuario Fue Registrado Exitosamente", 
      "data" : resul
    })
    //VALIDAR LOS CAMPOS
  }

  async logout ({ auth, request, response }) {

    const apiToken = auth.getAuthHeader()
    console.log(apiToken); 

    await auth
    .authenticator('jwt')
    .revokeTokens([apiToken], true)

    return response.ok({
      "status" : true, 
      "massage" : "El Usuario Fue Registrado Exitosamente", 
      "data" : auth.check()
    })
    
  }

  async signIn ({request, response}){
    const userData = request.only(User.store);

    const user = await User.create(userData);

    const rol =  await Rol.findBy({name : 'invitado'})

    const r = await user.Rol().associate(rol)

    return response.ok({
      "status" : true, 
      "massage" : "Te has registrado con exito.", 
      "data" : user
    });
  }
}

module.exports = AuthController
