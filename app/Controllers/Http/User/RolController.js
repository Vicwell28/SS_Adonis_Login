'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Rol = use('App/Models/Rol');

/**
 * Resourceful controller for interacting with rols
 */
class RolController {
  /**
   * Show a list of all rols.
   * GET rols
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {

    try{
      const roles = await Rol.all(); 
    
      return response.status(200).json({
        "status" : true, 
        "massage" : "Estos son los roles", 
        "data" : roles
      })
    }
    catch (error){
      return response.status(200).json({
        "status" : true, 
        "massage" : "Estos son los roles", 
        "data" : error
      })
    }
  }

  /**
   * Create/save a new rol.
   * POST rols
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const rolData = request.only(Rol.store)

    const rols = await Rol.create(rolData)

    return response.status(200).json({
      "status" : true, 
      "massage" : "El Usuario Fue Registrado Exitosamente", 
      "data" : rols
    })

  }

  /**
   * Display a single rol.
   * GET rols/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }


  /**
   * Update rol details.
   * PUT or PATCH rols/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a rol with id.
   * DELETE rols/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = RolController
