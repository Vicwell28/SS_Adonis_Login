'use strict'

const Validator = use('Validator')
const Database = use('Database')

class SignInAuth {
  get validateAll () {
    return true
  }

  async fails (errorMessages) {
    console.log(errorMessages); 
    return this.ctx.response.send(errorMessages)
  }

  get rules () {
    return {
      username : 'required|min:3|max:25',
      email: 'required|email|unique:users,email',
      password: 'required'
    }
  }

  get messages () {
    return {
      'username.min' : 'El nombre de usuario debe tener minimo 3 letras.', 
      'username.max' : 'El nombre de usuario debe tener maxiomo 25 letras.', 
      'username.required' : 'No puedes dejar el nombre de usuario vacio.',
      'email.required': 'No puedes dejar el campo correo electronico vacio.',
      'email.unique': 'El correo electronico ya esta registrado',
      'email.email': 'Debes colocar un correo electronico valido.',
      'password.required': 'No puede dejar el campo de contaseña vacio.'
    }
  }
}

module.exports = SignInAuth
