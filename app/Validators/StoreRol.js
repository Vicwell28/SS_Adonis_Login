'use strict'

class StoreRol {
  get validateAll () {
    return true
  }

  async fails (errorMessages) {
    console.log(errorMessages); 
    return this.ctx.response.send(errorMessages)
  }

  get rules () {
    return {
      name : 'required|min:3|max:25'
    }
  }

  get messages () {
    return {
      'name.min' : 'El rol debe tener minimo 3 letras.', 
      'name.max' : 'El rol debe tener maxiomo 25 letras.', 
      'name.required' : 'No puedes dejar el rol vacio.',
    }
  }
}

module.exports = StoreRol
